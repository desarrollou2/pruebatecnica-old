Prueba t�cnica U2

- Se debe configurar la base de datos haciendo uso de los siguientes datos de acceso:
   host: 192.168.0.231
   user: pruebatecnica
   password: U22018
   database: pruebatecnica
   
- Se debe crear un CRUD de usuarios teniendo en cuenta que se deben capturar todos los campos de la tabla users. El campo users.users_cod_perfil se relaciona con la tabla perfiles.
- Crear un login sencillo y habilitar el acceso al CRUD de usuarios solo para los usuarios que tienen una sesi�n iniciada.

- Si se har� uso de Laravel, se debe instalar y configurar el proyecto. En caso de no usar Laravel, el proyecto debe usar POO y arquitectura MVC.
- El dise�o de la interfaz y el uso de Javascript es libre, por lo que pueden usar las librer�as que se necesiten para su funcionamiento.